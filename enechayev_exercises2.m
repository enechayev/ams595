%Eric Nechayev - 108952140 - Assignment 3

%Number 1
x=-2:0.2:2
y1=x.^0.5;
y2=x.^1;
y3=x.^2;
y4=x.^3;
plot(x,y1,'--',x,y2,':',x,y3,'b--o',x,y4);

%Number 2
subplot(2,2,1);
X1=10*rand(100,1);
E=rand()-1;
Y1=2*X1+E;
plot(X1,Y1,':rX');

subplot(2,2,2);
X2=5*rand(50,1)-5;
Y2=sqrt(5-(X2.^2));
plot(X2,Y2,'.b','MarkerSize',5);

subplot(2,2,3);
X3=[-10:10];
Y3=normpdf(X3,1,2);
plot(X3,Y3);

subplot(2,2,4);
X4=[0 0 2 2 0];
Y4=[0 2 2 0 0];
plot(X4,Y4,'-o');


%Number 3
x=2*rand(100,1)-2;
y=2*rand(100,1)-2;
z=2*rand(100,1)-2;
d=sqrt(x.^2+y.^2+z.^2);
plot3(x,y,z,'MarkerSize',10);

%Number 4
[x,y,z]=sphere;
surf(4*x,4*y,4*z)