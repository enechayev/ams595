%Eric Nechayev
%108952140
%Bitbucket: enechayev

%---------------------------------------------------------------
%TESTING THE CODE:

%mcPi(10000)
%errorGraph(1000)
%presComp(5000)
%maxPrecision(2)
%maxPrecision(3)
%---------------------------------------------------------------
%Question #1

%Basic FOR Loop Pi Computation Function.
function piSampleVal = mcPi(numRandoms)
    inSubCircle = 0;
    for i= 1:numRandoms
        mcX = rand(numRandoms,1);
        mcY = rand(numRandoms,1);
        %Binary list of constraint satisfiers.
        inSubCircle=sum(and(mcX<=sqrt(1-mcY.^2),mcY<=sqrt(1-mcX.^2)));
        piSampleVal=4*inSubCircle/numRandoms;
    end
end

%Error Graph.
function A = errorGraph(numTrials)
    errorList=[];
    for j=1:numTrials
        difference=pi-mcPi(j);
        errorList=[errorList;difference];
    end
    l=1:numTrials
    m=pi
    %Graphs true pi value, converging approximation from greater sample, and error from true value. 
    A=plot(1:numTrials,errorList,1:numTrials,pi-errorList,l,m,'-g.')
    title("Monte Carlo Error Convergence of Pi")
    xlabel("Number of Random Sample Points")
    ylabel("Estimate")
end


%Precision versus Computational Cost graph.
function presComp(maxTrials)
    computationalCosts = [];
    precisions = [];
    %Sets base for time count.
   
    for j=100:100:maxTrials
        tic
        %Basic error, calls the computation function, finds error, adds to list.
        precisions = [precisions;abs((mcPi(j)-pi))];
        %Gets a new time as it runs through the loop.
        computationalCosts = [computationalCosts;toc]; 
    end
    %Subtracts initial time from all times.
    %computationalCosts = computationalCosts-t0;
    plot(precisions,computationalCosts)
    title("Convergence of Computing Efficiency")
    xlabel("Precision")
    ylabel("computationalCosts")
end


%-------------------------------------------------------------
%Question #2/#3

%maxPrecision(2)
%maxPrecision(3)

%WHILE LOOP Method: 
%user gives a level of precision in form of significant figures,
%and is returned an approximation of Pi and the number of random 
%points it took to get something as close as they wanted it.
function [piApprox,trialsItTook] = maxPrecision(numSigFigs)
    numRandoms=250;
    inSubCircle = 0;
    standardError=.25;
    precisionLevel=10^-(numSigFigs);
    
    while standardError>precisionLevel
        mcX = rand(numRandoms,1);
        mcY = rand(numRandoms,1);
        inSubCircle=and(mcX<=sqrt(1-mcY.^2),mcY<=sqrt(1-mcX.^2));
        standardError=std(inSubCircle*4)/sqrt(numRandoms);
        numRandoms=numRandoms+250;
    end
    a=mcX(mcX<=sqrt(1-mcY.^2));
    b=mcY(mcY<=sqrt(1-mcX.^2));
    xCir=0:.01:1;
    yCir=sqrt(1-xCir.^2);
    plot(mcX,mcY,'d',xCir,yCir,'m',a,b,'g')
    title("Monte Carlo Random Point Sample")
    xlabel("x")
    ylabel("y")
    piApprox = 4*sum(inSubCircle)/numRandoms
    trialsItTook = numRandoms
end