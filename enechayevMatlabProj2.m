%Eric Nechayev
%Bitbucket ID: enechayev
%Stony Brook ID: 108952140

A = [3 5 8 3; 5 7 1 9; 2 1 6 9; 3 4 3 7];
%A = [0 8 1 7 5; 3 9 7 1 4; 5 0 3 2 4];
b = [1 2 3 4].';
%TESTING:
%rowEchelon(A);
%rowCanonical(A);
%rowCanonical(rowEchelon(A));
%whichForm(rowCanonical(A));
%whichForm(rowEchelon(A));
%systemSolve(A,b);
%solveTime()
%matrixInverse(A);
%LU_decomposition(A);

function formType = whichForm(A)
    formType="";
    pivots=[];
    for j = 1:size(A,1)-1
        pivots=[pivots;A(j,j)];
        colJ=A(j+1:end,j);
        if colJ==0
        	formType="This matrix is in Row Echelon Form.";	
        end
    end   
    if pivots(:)==1
    	formType="This input matrix is in Row Canonical Form.";
    end    
    if and(formType~="This matrix is in Row Echelon Form.",formType~="This matrix is in Row Canonical Form.")
        formType = "This input matrix is NEITHER Row Echelon nor Row Canonical.";
    end
    disp(formType);
end

function [rowEchForm,M,P,elemProd] = rowEchelon(A)
    disp("Original Matrix is:")
    disp(A)
    M = eye(size(A,1),size(A,2));
    P = eye(size(A,1),size(A,2));
    elemProd = eye(size(A,1),size(A,2));
    
    for i = 1:size(A,1)
        for j = 1:size(A,2)
            if A(i,j)==0
                for k=i:size(A,1)
                    if A(k,j)~=0
                        A([i,k],:) = A([k,i],:);
                        RE = eye(size(A,1),size(A,2));
                        RE([i,k],:) = RE([k,i],:);
                        if and(size(P,1)==size(RE,2),size(RE,2)==size(elemProd,1))
                            P=RE*P;
                            elemProd=RE*elemProd;
                        end
                    end
                end
            end
            if A(i,j)~=0 %A_ij becomes the Pivot.
                for k=i+1:size(A,1)
                    multiplier = (-A(k,j)/A(i,j));
                    A(k,:) = A(k,:) +  multiplier * A(i,:);
                    E3 = eye(size(A,1),size(A,2));
                    E3(k,:) = E3(k,:) + multiplier * E3(i,:);
                    if and(size(M,1)==size(E3,2),size(E3,2)==size(elemProd,1))
                        M=E3*M;
                        elemProd=E3*elemProd;
                    end
                end
                if i~=size(A,1)
                    i=i+1;
                    j=j+1;
                end
                if i==size(A,1) 
                    echelonForm = "Row Echelon Form is:";
                end
            end   
        end
    end
    
    disp("Row Elementary Product:");
    disp(elemProd);
    disp(echelonForm);
    rowEchForm=A;
    disp(rowEchForm);
end

function [rowCanForm,canElemProd] = rowCanonical(A)
    rowCanForm = A;
    canElemProd=eye(size(A,1),size(A,2));
    disp("Original Matrix:")
    disp(A)
    if whichForm(A)~="This matrix is in Row Echelon Form."
        disp("There's an error - your input was not in row echelon form.");
        return;
    end
    
    for i = size(A,1):-1:1
        for j = size(A,2):-1:1
            if A(i,j)~=0
                E2 = eye(size(A,1),size(A,2));
                E2(i,:)=E2(i,:)*(1/A(i,j));
                A(i,:)=A(i,:)*(1/A(i,j));
                if size(E2,2)==size(canElemProd,1)
                    canElemProd=E2*canElemProd;
                end
                for k=i-1:-1:1
                    E3 = eye(size(A,1),size(A,2));
                    E3(k,:) = E3(k,:) + (-A(k,j)*E3(i,:));
                	A(k,:)=A(k,:) + (-A(k,j)*A(i,:));
                    if size(E3,2)==size(canElemProd,1)
                        canElemProd=E3*canElemProd;
                    end
                end  
            end
        end
    end
    disp("Canonical Elementary Product:");
    disp(canElemProd);
    rowCanForm=A;
    disp("Row Canonical Form is:")
    disp(rowCanForm);
end

function inverse = matrixInverse(A)
    if (and(size(A,1)==size(A,2),det(A)~=0))
        [rowEchForm,rowEchM,rowEchP,rowElemProd]=rowEchelon(A);
        [rowCanForm,canElemProd]=rowCanonical(rowEchForm);
        %Inverse is product of ALL elementary matrices (Both forward elimination and back substitution).
        finalElemProd = canElemProd*rowElemProd;
        disp("The inverse is:")
        inverse = finalElemProd;
        fileID=fopen('invFile.txt','w');
        fprintf(fileID,'%f',inverse);
        fclose(fileID);
        disp(inverse);
    elseif det(A)==0
            disp("Error: The input matrix is singular, and thus uninvertible.");
            return;
    else disp("Error: The input matrix is not square, and thus uninvertible.");
        return;
    end
end

function [L,U,P] = LU_decomposition(A)
    [~,rowEchM,rowEchP,~]=rowEchelon(A);
    M=rowEchM;
    %Lower Triangular
    L=matrixInverse(M);
    %Upper Triangular
    U=M*A;
    %Permutation Matrix
    P=rowEchP;
    disp("L=");
    disp(L);
    disp("U=");
    disp(U);
    disp("P=");
    disp(P);
end

function axbSoln = systemSolve(A,b)
     if size(A,2)~=size(A,1)
        disp("Error: This kind of input matrix has no unique solution.");
        axbSoln = 0;
     else
        augmented = [A, b];
        echelonAug = rowCanonical(rowEchelon(augmented));
        axbSoln = echelonAug(:,end);
        for n =1:size(A,2)
            disp("x"+n+" = "+axbSoln(n))
        end
     end
end

function solveTime
    times=[];
    n=100;
    for dimRow = 1:n
            dimCol=dimRow;
            v=rand(dimRow,1);
            U=rand(dimRow,dimCol);
            tic; %Storing the dimension-corresponding computation time in an array.
            systemSolve(U,v);
            times=[times;toc];
    end
    dimRowSize=[1:n];
    dimColSize=[1:n];
    figure;
    scatter3(dimRowSize,dimColSize,times,'filled');
    title('Exponentially Slower Linear System Solving');
    xlabel('# of Rows');
    ylabel('# of Columns');
    zlabel('Computation Time (sec)');
    disp(times);
end