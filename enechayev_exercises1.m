%Eric Nechayev
%SB ID: 108952140
%Bitbucket ID: enechayev

%1
a = zeros(10,10);
for i = 1 : 10
    for j = 1 : 10
        a(i,j) = i^2 + j^2;
    end
end


%2
b = zeros(12,12);
for i = 1 : 12
    for j = 1 : 12
        if j>=i
            b(i,j) = i*j;
        else
            b(i,j) = 0;
        end
    end
end

%3
c = zeros(8,8);
for i = 1 : 8
    for j = 1 : 8
        if i==j
            c(i,j) = 0;
        else
            c(i,j) = 1/abs(i-j);
        end
    end
end

%4
M = rand(3,50);

%5
A = zeros(3,50);
for i = 1 : 3
	for j = 1: 50
        A(i,j) = M(i,j)^2;
    end
end

plot(A,M,'.-');

%6
B = zeros(3,50);
for i = 1 : 3
	for j = 1: 50
        if M(i,j)>0.1
        	B(i,j)=M(i,j);
        end
        if M(i,j)<0
        	B(i,j)=0;
        end
    end
end

norm(M-B);

%7
x = linspace(2,200,100);

%8
u = zeros(1,100);
v = zeros(1,100);
w = zeros(1,100);

for i = 1 : 100
    u(i) = exp(x(i));
    v(i) = log(x(i));
    w(i) = sqrt((100-x(i)^2));
end

%9
plot(x,u,x,v,x,w);

